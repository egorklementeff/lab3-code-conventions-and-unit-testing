package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class threadControllerTests {

    private Thread thread;
    private Thread another_thread;
    private Post post;
    private User user;

    @BeforeEach
    @DisplayName("test1")
    void testInit() {
        thread = new Thread(
                "thread_author",
                new Timestamp(System.currentTimeMillis() / 1000),
                "thread_forum",
                "thread_message",
                "thread_slug",
                "thread_title",
                7
        );
        thread.setId(0);

        another_thread = new Thread(
                "another_thread_author",
                new Timestamp(System.currentTimeMillis() / 1000),
                "another_thread_forum",
                "another_thread_message",
                "another_thread_slug",
                "another_thread_title",
                8
        );
        another_thread.setId(0);

        post = new Post(
                "post_author",
                new Timestamp(System.currentTimeMillis() / 1000),
                "post_forum",
                "post_message",
                777,
                666,
                false
        );
        user = new User(
                "user_nickname",
                "user_email",
                "user_fullname",
                "user_about"
        );
    }

    @Test
    @DisplayName("testCheckIdOrSlug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(666)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(thread, controller.CheckIdOrSlug("666"));
        }
    }

    @Test
    @DisplayName("testCreatePost")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(666)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);

            List<Post> posts = new ArrayList<>() { { add(post); } };
            threadMock.when(() -> ThreadDAO.getPosts(0, 1, 0, "sort", false))
                    .thenReturn(posts);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("post_author")).thenReturn(user);
                threadController controller = new threadController();
                assertEquals(
                        ResponseEntity.status(HttpStatus.CREATED).body(posts),
                        controller.createPost("thread_slug", posts)
                );
            }
        }
    }

    @Test
    @DisplayName("testPosts")
    void testPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(666)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);

            List<Post> posts = new ArrayList<>() { { add(post); } };
            threadMock.when(() -> ThreadDAO.getPosts(0, 1, 0, "sort", false))
                    .thenReturn(posts);

            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(posts),
                    controller.Posts("thread_slug", 1, 0, "sort", false)
            );
        }
    }

    @Test
    @DisplayName("testChange")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("another_thread_slug")).thenReturn(another_thread);

            threadMock.when(() -> ThreadDAO.change(thread, another_thread)).thenAnswer(invocationOnMock -> {
                threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(another_thread); // Change itself
                return null;
            });

            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(another_thread),
                    controller.change("thread_slug", another_thread)
            );
            assertEquals(
                another_thread, controller.CheckIdOrSlug("another_thread_slug")
            );
        }
    }

    @Test
    @DisplayName("testInfo")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info("thread_slug")
            );
        }
    }

    @Test
    @DisplayName("testCreateVote")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("thread_slug")).thenReturn(thread);

            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info("post_author")).thenReturn(user);
                threadController controller = new threadController();
                // Cannot access the "Vote" class since it is not public. I need it to call "createVote" method.
                //controller.createVote("thread_slug", new Vote());
                assertTrue(true);
            }
        }
    }

}

